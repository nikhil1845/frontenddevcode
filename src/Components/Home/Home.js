import React, { Component } from "react";
import "./Home.css";
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }
  showDetails = (e) => {
    console.log("e", e.currentTarget.id);
    const selectedUser = this.state.response.results.find(
      (user) => user.email === e.currentTarget.id
    );
    this.setState({ selectedUser: selectedUser });
  };
  fetchUsers = () => {
    fetch("https://randomuser.me/api/?results=8")
      .then((results) => {
        return results.json();
      })
      .then((data) => {
        this.setState({ isLoading: false });
        let pictures = data.results.map((pic) => {
          return (
            <div key={pic.results} id={pic.email} onClick={this.showDetails}>
              <img style={{ width: 150 }} src={pic.picture.thumbnail} />
            </div>
          );
        });
        this.setState({
          pictures: pictures,
        });
        this.setState({
          response: data,
        });
      });
  };
  componentDidMount() {
    this.fetchUsers();
  }
  render() {
    const { selectedUser } = this.state;
    return (
      <div className="container-1">
        <div className="box-1">
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
            }}
          >
            <p style={{ fontSize: 10 }}>CORPORATE NEWS SUBSCRIPTION</p>
            <p style={{ fontSize: 20 }}>THE AUSTRELIAN</p>
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              flexWrap: "wrap",
            }}
          >
            <div>
              <h2 style={{ width: 350 }}>
                The future of your business relies on being informed
              </h2>
              <button
                style={{ height: 40, backgroundColor: "blue", color: "white" }}
              >
                Request a quote
              </button>
            </div>
          </div>
        </div>

        <p>or Contact a representative below</p>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <div className="imageGrid">
            {!this.state.isLoading ? this.state.pictures : <h1>Loading...</h1>}
          </div>
        </div>
        <div style={{ padding: 20 }}>
          <button
            style={{
              height: 40,
              width: 120,
              backgroundColor: "blue",
              color: "white",
            }}
            onClick={(e) => {
              this.setState({ isLoading: true });
              this.fetchUsers();
            }}
          >
            View More >
          </button>
        </div>
        {selectedUser && (
          <div>
            <h3 style={{ textAlign: "left" }}>You are viewing:</h3>
            <div className="box-2">
              <div style={{ textAlign: "left" }}>
                <h4>
                  {selectedUser.name.title +
                    " " +
                    selectedUser.name.first +
                    " " +
                    selectedUser.name.last}
                </h4>
              </div>
              <div className="box-3">
                <div
                  className="box-3-div1"
                  style={{
                    borderStyle: "none",
                  }}
                >
                  <img src={selectedUser.picture.thumbnail} />
                </div>
                <div
                  className="box-3-div2"
                  style={{
                    textAlign: "left",
                    marginLeft: 10,
                    borderStyle: "none",
                  }}
                >
                  <h5>{selectedUser?.login?.username}</h5>

                  <h5>{selectedUser?.location?.street.name}</h5>
                </div>
              </div>
              <div style={{ textAlign: "left" }}>
                <h4>{selectedUser.email}</h4>
              </div>
              <div style={{ textAlign: "left" }}>
                <h4>{selectedUser.phone}</h4>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Home;
